/*
 * Copyright (C) 2025 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.actions;

import org.openxava.actions.ViewBaseAction;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class Notifier extends ViewBaseAction {

  private String tipoDocumento;
  
  @Override
  public void execute() throws Exception {
/*    String[] requiredRoles = {Role.ADMIN.label, Role.TECNICO.label};
    if (!Role.userHasAnyRoleOf(requiredRoles)) {
      throw new UnsupportedOperationException("Non puoi inviare notifiche");
    }
    EntityManager em = XPersistence.createManager();
    EntityTransaction et = em.getTransaction();
    try {
      et.begin();
      Logger.getLogger(getClass().getName()).
             log(Level.WARNING, "Richiesto invio email notifica di caricamento " + tipoDocumento);
      Edificio edi = (Edificio)getView().getModel();
      RPF r = edi.getRpf();
      if (tipoDocumento.equalsIgnoreCase("poa")) {
        inviaPOA(em, r);
      } else if (tipoDocumento.equalsIgnoreCase("lds")) {
        inviaLivelli(em, r);
      } else if (tipoDocumento.equalsIgnoreCase("vdc")) {
        inviaVdC(em, r);
      }
    } finally {
      if (et.isActive())
        et.rollback();
      em.close();      
    }    
  }

  public String getTipoDocumento() {
    return tipoDocumento;
  }

  public void setTipoDocumento(String tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }
  
  private ModelloNotificaInserimentoDoc trovaModello(EntityManager em, ModelloNotificaInserimentoDoc.Evento evento) {
    TypedQuery<ModelloNotificaInserimentoDoc> mnidq = em.createQuery("SELECT m FROM ModelloNotificaInserimentoDoc m WHERE m.evento = :evento", ModelloNotificaInserimentoDoc.class);
    mnidq.setParameter("evento", ModelloNotificaInserimentoDoc.Evento.INSERIMENTO_POA);
    mnidq.setMaxResults(1);
    ModelloNotificaInserimentoDoc modello = mnidq.getSingleResult();
    return modello;
  }

  private List<EsecutoriDelServizio> trovaEsecutori(EntityManager em, RPF r) {
    Collection<Incarico> incs = r.getIncarichi();
    LinkedList<EsecutoriDelServizio> esecs = new LinkedList<>();
    for (Incarico i: incs) {
      esecs.add(i.getEsecutori());
    }
    return esecs;
  }
  
  private List<String> trovaDestinatari(EntityManager em, RPF r) {
    ModelloNotificaInserimentoDoc m = trovaModello(em, 
            ModelloNotificaInserimentoDoc.Evento.INSERIMENTO_POA);
    List<EsecutoriDelServizio> esec = trovaEsecutori(em, r);
    List<String> utenti = new LinkedList<>();
    for (EsecutoriDelServizio eds: esec)
      utenti.add(eds.getOwnerUsername());
    utenti.add(r.getEnte().getOwnerUsername());
    TypedQuery<Account> accq = em.createQuery("SELECT a FROM Account a WHERE a.nomeUtente IN (:nomi)", Account.class);
    accq.setParameter("nomi", utenti);
    Stream<Account> accs = accq.getResultStream();
    Stream<String> emails = accs.map(account -> account.getEmail());
    return emails.toList();
  }


  private void inviaPOA(EntityManager em, RPF r) {
    invia(em, r, ModelloNotificaInserimentoDoc.Evento.INSERIMENTO_POA);
  }

  private void inviaVdC(EntityManager em, RPF r) {
    invia(em, r, ModelloNotificaInserimentoDoc.Evento.INSERIMENTO_VdC);
  }

  private void inviaLivelli(EntityManager em, RPF r) {
    invia(em, r, ModelloNotificaInserimentoDoc.Evento.INSERIMENTO_LIVELLI_DI_SERVIZIO);
  }

  private void invia(EntityManager em, RPF r, ModelloNotificaInserimentoDoc.Evento ev) {
    List<String> destinatari = trovaDestinatari(em, r);
    ModelloNotificaInserimentoDoc mod = trovaModello(em, ev);
    LocalDate now = LocalDate.now();
    String subj = compilaCampi(mod.getOggetto(), r, now);
    String text = compilaCampi(mod.getTesto(), r, now);
    for (String dest: destinatari)
      Emailer.send(dest, subj, text);
*/  }
  
}
