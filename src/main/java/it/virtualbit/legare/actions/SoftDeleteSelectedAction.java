/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.actions;

import static it.virtualbit.legare.actions.SoftDeleteAction.deletedProperty;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.openxava.actions.IChainActionWithArgv;
import org.openxava.actions.TabBaseAction;
import org.openxava.model.MapFacade;
import org.openxava.model.meta.MetaModel;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class SoftDeleteSelectedAction extends TabBaseAction // To work with tabular data (list) by means of getTab()
        implements IChainActionWithArgv {

  private String nextAction = null; // To store the next action to execute

  @Override
  public void execute() throws Exception {
    if (!getMetaModel().containsMetaProperty(deletedProperty)) {
      nextAction = "CRUD.deleteSelected"; // 'CRUD.deleteSelected' will be
      return;    // executed after this action is finished
    }
    markSelectedEntitiesAsDeleted(); // The logic to mark the selected rows
    // as deleted objects
  }

  private MetaModel getMetaModel() {
    return MetaModel.get(getTab().getModelName());
  }

  @Override
  public String getNextAction() // Required because of IChainAction
          throws Exception {
    return nextAction; // If null no action will be chained
  }

  @Override
  public String getNextActionArgv() throws Exception {
    return "row=" + getRow(); // Argument to send to chainged action
  }

  private void markSelectedEntitiesAsDeleted() throws Exception {
    Map<String, Object> values = new HashMap<>(); // Values to assign to each entity to be marked
    values.put(deletedProperty, new Date()); // Just set deleted to true
    Map<String, Object>[] selectedOnes = getSelectedKeys(); // We get the selected rows
    if (selectedOnes != null)
      for (int i = 0; i < selectedOnes.length; i++) { // Loop over all selected rows
        Map<String, Object> key = selectedOnes[i]; // We obtain the key of each entity
        try {
          MapFacade.setValues( // Each entity is modified
                  getTab().getModelName(),
                  key,
                  values);
        } catch (javax.validation.ValidationException ex) { // If there is a ValidationException...
          addError("no_delete_row", i, key);
          addError("remove_error", getTab().getModelName(), ex.getMessage()); // ...we show the message
        } catch (Exception ex) { // If any other exception is thrown, a generic
          addError("no_delete_row", i, key); // message is added
        }
      }
    getTab().deselectAll(); // After removing we deselect the rows
    resetDescriptionsCache(); // And reset the cache for combos for this user
  }
}
