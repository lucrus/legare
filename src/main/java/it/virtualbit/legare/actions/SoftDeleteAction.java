/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.actions;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.openxava.actions.ViewBaseAction;
import org.openxava.model.MapFacade;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class SoftDeleteAction extends ViewBaseAction {

  public static final String deletedProperty = "deletedAt";
  
  @Override
  public void execute() throws Exception {
    if (!getView().getMetaModel().containsMetaProperty(deletedProperty)) {
      executeAction("CRUD.delete"); // We call the standard OpenXava
      return;                       //   action for deleting
    }
    Map<String, Object> values
            = new HashMap<>(); // The values to modify in the entity
    values.put(deletedProperty, new Date()); // We set true to 'deletedAt' property
    MapFacade.setValues( // Modifies the values of the indicated entity
            getModelName(), // A method from ViewBaseAction
            getView().getKeyValues(), // The key of the entity to modify
            values); // The values to change
    resetDescriptionsCache(); // Clears the caches for combos
    addMessage("object_deleted", getModelName());
    getView().clear();
    getView().setKeyEditable(true);
    getView().setEditable(false); // The view is left as not editable  }
  }
}
