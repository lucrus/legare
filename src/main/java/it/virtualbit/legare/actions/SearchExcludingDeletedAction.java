/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.actions;

import static it.virtualbit.legare.actions.SoftDeleteAction.deletedProperty;
import java.util.Map;
import javax.ejb.ObjectNotFoundException;
import org.openxava.actions.SearchByViewKeyAction;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class SearchExcludingDeletedAction
        extends SearchByViewKeyAction { // The standard OpenXava action to search

  private boolean isDeletable() { // To see if this entity has a deleted property
    return getView().getMetaModel()
            .containsMetaProperty(deletedProperty);
  }

  @Override
  protected Map getValuesFromView() // Gets the values displayed in this view
          throws Exception // These values are used as keys for the search
  {
    return isDeletable() ? addDeletedKey(super.getValuesFromView()) : super.getValuesFromView();
  }

  @Override
  protected Map getMemberNames() // The members to be read from the entity
          throws Exception {
    return isDeletable() ? addDeletedKey(super.getMemberNames()) : super.getMemberNames();
  }
  
  private Map<String, Object> addDeletedKey(Map<String, Object> map) {
    map.put(deletedProperty, null);
    return map;
  }

  @Override
  protected void setValuesToView(Map values) // Assigns the values from the
          throws Exception // entity to the view
  {
    if (isDeletable()
            && // If it has an deleted property and
            values.get(deletedProperty) != null) // it is set
      throw new ObjectNotFoundException(); // The same exception OpenXava // throws when the object is not found
    else
      super.setValuesToView(values); // Otherwise we run the standard logic
  }
}
