/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.actions;

import it.virtualbit.legare.model.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import org.openxava.jpa.XPersistence;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class ScadenzeProvisioner {
  
  private List<Adempimento> getAde(Scadenzabile impegno, Commessa c) {
    Adempimento.ClasseRif cr = impegno instanceof RPF ? Adempimento.ClasseRif.RPF : Adempimento.ClasseRif.OPF;
    String q = c == null ? "SELECT a FROM Adempimento a WHERE a.relativoA = :tipologia" :
                           "SELECT a FROM Adempimento a WHERE a.relativoA = :tipologia AND a.commessa = :comm";
    TypedQuery<Adempimento> qa = XPersistence.getManager().createQuery(q, Adempimento.class);
    qa.setParameter("tipologia", cr);
    if (c != null)
      qa.setParameter("comm", c);
    return qa.getResultList();
  }
  
  public List<Scadenza> crea(Scadenzabile impegno) {
    // cerco gli adempimenti per la commessa specifica
    Commessa c = impegno.retrieveCommessa();
    List<Adempimento> al = getAde(impegno, impegno.retrieveCommessa());
    if (al.isEmpty()) // se non li trovo, prendo quelli di default per tutte le commesse
      al = getAde(impegno, null);
    return this.crea(al, impegno);
  }
  
  public List<Scadenza> crea(List<Adempimento> al, Scadenzabile impegno) {
    LinkedList<Scadenza> result = new LinkedList<>();
    for (Adempimento a: al) {
      Scadenza s = new Scadenza();
      String testoImpegno = impegno.getClass().getSimpleName() + " - " + impegno.getTestoScadenze();
      s.setImpegno(testoImpegno);
      s.setAdempimento(a);
      s.setIdDocumentoCollegato(impegno.getUuid());
      s.setTipoDocumento(impegno instanceof RPF ? Adempimento.ClasseRif.RPF : Adempimento.ClasseRif.OPF);
      
      LocalDate dataScadenza = this.calcolaDataScadenza(impegno, a);
      s.setIl(dataScadenza);
      s.setAvvisi(this.creaAvvisi(a, s, testoImpegno));
      s.setDescrizione(a.getNome() + " - " + s.getImpegno());
      // forzo l'inizializzazione dei campi, perché qui siamo nella PostPersist
      // (relativa all'RPF), quindi la PrePersist sulle altre entities non sarà
      // più chiamata nella commit che ci sarà (se ho capito bene)
      s.initAutomaticColumns();
      result.add(s);
    }
    return result;
  }
  
  private LocalDate calcolaDataScadenza(Scadenzabile i, Adempimento a) {
    LocalDate da = i.getDataIniziale();
    LocalDate oggi = LocalDate.now();
    if (a.getCalcolo() == Adempimento.Origine.GIORNO_DEL_MESE) {
      if (a.getRicorrenza() == Adempimento.Ricorrenza.OGNI_MESE 
              && da.isBefore(oggi) &&
              i.getDataFinale().isAfter(oggi))
        da = oggi;
      da = da.minusDays(da.getDayOfMonth());
    }
    return da.plusDays(a.getGiorni());
  }

  private List<Avviso> creaAvvisi(Adempimento ad, Scadenza s, String testo) {
    List<Avviso> result = new LinkedList<>();
    for (ModelloAvvisoScadenza ma: ad.getAvvisi()) {
      Avviso a = new Avviso();
      a.setDataAttivazione(s.getIl().minusDays(ma.getGiorniAnticipo()));
      a.setOggetto(ma.getOggetto());
      a.setTesto(ma.getTesto() + "\n(Rif.: " + testo + ")");
      a.setScadenza(s);
      a.setAzione(ma.getAzione());
      // forzo l'inizializzazione dei campi, perché qui siamo nella PostPersist
      // (relativa all'RPF), quindi la PrePersist sulle altre entities non sarà
      // più chiamata nella commit che ci sarà (se ho capito bene)
      a.initAutomaticColumns();
      result.add(a);
    }
    return result;
  }
  
  public void salva(List<Scadenza> ls) {
    EntityManager em = XPersistence.createManager();
    EntityTransaction et = em.getTransaction();
    try {
      et.begin();
      for (Scadenza s: ls) {
        System.out.println("Creo scadenza " + s.getDescrizione() + " (id doc " + s.getIdDocumentoCollegato() + ")");
        em.persist(s);
        for (Avviso a: s.getAvvisi()) {
          System.out.println("Creo avviso " + a.getOggetto());
          em.persist(a);
        }
      } 
      et.commit();
    } finally {
      if (et.isActive())
        et.rollback();
    }
  }
}
