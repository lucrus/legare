package it.virtualbit.legare.run;

import org.openxava.util.AppServer;
import org.openxava.util.DBServer;

/**
 * Execute this class to start the application.
 *
 * With OpenXava Studio/Eclipse: Right mouse button > Run As > Java Application
 */

public class legare {
  

	public static void main(String[] args) throws Exception {
    
		DBServer.start("legare-db"); // To use your own database comment this line and configure src/main/webapp/META-INF/context.xml
		AppServer.run("legare"); // Use AppServer.run("") to run in root context
	}

}
