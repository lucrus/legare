/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.editors;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import org.openxava.web.editors.AttachedFile;
import org.openxava.web.editors.FileSystemPersistor;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class UploadedFileRenamer extends FileSystemPersistor {
  @Override
    public void save(AttachedFile file) {
        final String currentName = file.getName();
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
        final String separator = "#";
        boolean dataDaAggiungere = true;
        final int idx = currentName.indexOf(separator);
        if (idx >= 0) {
          final String strDate = currentName.substring(0, idx);
          try {
            LocalDateTime.parse(strDate, formatter);
            dataDaAggiungere = false;
          } catch (DateTimeParseException e) {
          }
        }
        if (dataDaAggiungere) {
          file.setName(LocalDateTime.now().format(formatter) + separator + currentName);         
        }
        super.save(file);
    }
}
