/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.editors;

import javax.servlet.http.HttpServletRequest;
import org.openxava.formatters.BaseFormatter;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class UrgenzaFormatter extends BaseFormatter{

  @Override
  public String format(HttpServletRequest hsr, Object o) {

    String result = (String)o;
    try {
      String[] classiCss = {"scaduta", "urgentissima", "urgente", "imminente", "normale", "evasa"};
      int indiceUrgenza = Integer.parseInt(result.substring(0, 1));
      String css = "legare-" + classiCss[indiceUrgenza];
      result = "<span class=\"" + css + "\">" + result + "</span>";
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    return result;
  }
  
  @Override
  public Object parse(HttpServletRequest hsr, String string) throws Exception {
    return string;
  }
  
}
