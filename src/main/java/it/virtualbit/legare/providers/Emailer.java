package it.virtualbit.legare.providers;

import it.virtualbit.legare.model.*;
import it.virtualbit.legare.model.redis.Role;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.apache.commons.mail2.core.EmailException;
import org.apache.commons.mail2.javax.HtmlEmail;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
public class Emailer {
  public static boolean send(String dest, String subj, String body) {
    try {
      HtmlEmail mail = new HtmlEmail();
      mail.setCharset(StandardCharsets.UTF_8.name());
      mail.setHostName("localhost");
      mail.setSmtpPort(25);
      mail.setSSLOnConnect(false);
      //mail.setAuthenticator(new DefaultAuthenticator("1a2b3c4d5e6f7g", "1a2b3c4d5e6f7g"));
      mail.setFrom("no-reply@ciclatfm.it", "CiclatFM");
      mail.addTo(dest, "");
      mail.setSubject(subj);
      mail.setMsg(body);
      
      mail.send();
    } catch (EmailException ex) {
      Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);
      return false;
    }
    return true;
  }  
  
  public static String compilaCampi(String testo, RPF r, LocalDate mese) {
    SimpleDateFormat sdfMese = new SimpleDateFormat("MMMM", Locale.ITALIAN);
    SimpleDateFormat sdfAnno = new SimpleDateFormat("YYYY", Locale.ITALIAN);
    
    LocalDate meseSucc = mese.plusMonths(1);
    
    return testo.replaceAll("\\{\\{codiceRPF\\}\\}", r.getCodice())
            .replaceAll("\\{\\{nomeCommessa\\}\\}", r.retrieveCommessa().getNome())
            .replaceAll("\\{\\{nomeEnte\\}\\}", r.getEnte().getDescrizione())
            .replaceAll("\\{\\{codiceLotto\\}\\}", r.getLotto().getCodice())
            .replaceAll("\\{\\{nomeMese\\}\\}", sdfMese.format(mese))
            .replaceAll("\\{\\{nomeMeseSuccessivo\\}\\}", sdfMese.format(meseSucc))
            .replaceAll("\\{\\{numeroAnno\\}\\}", sdfAnno.format(meseSucc))
            ;
  }  

  private static String emailDaNomeUtente(EntityManager em, String ownerUsername) {
    throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  }
  
  private static Collection<String> emailUtentiPerRuolo(EntityManager em, Role r) {    
    TypedQuery<Account> aq = em.createQuery("SELECT a FROM Account a WHERE a.ruoli LIKE :ruolo", Account.class);
    aq.setParameter("ruolo", "%" + r.toString() + "%");
    List<Account> alist = aq.getResultList();
    LinkedList<String> result = new LinkedList<>();
    for (Account a: alist) {
      String[] emails = a.getEmail().split(" ");
      result.addAll(Arrays.asList(emails));
    }
    return result;
  }

  public static Collection<String> emailAmministratori(EntityManager em) {
    return emailUtentiPerRuolo(em, Role.ADMIN);
  }

  public static Collection<String> emailTecnici(EntityManager em) {
    return emailUtentiPerRuolo(em, Role.TECNICO);
  }
  
  public static Collection<String> emailDaRichiesta(EntityManager em, Richiesta rich) {
    LinkedList<String> result = new LinkedList<>();
    result.add(emailDaNomeUtente(em, rich.getOwnerUsername()));
    Collection<Risposta> risposte = rich.getRisposte();
    for (Risposta r: risposte) {
      result.add(emailDaNomeUtente(em, r.getOwnerUsername()));
    }
    
    return result;
  }
  
  public static Collection<String> emailDaCommento(EntityManager em, Commento comm) {
    Richiesta r = null;
    if (comm instanceof Richiesta)
      r = (Richiesta)comm;
    else if (comm instanceof Risposta) {
      r = ((Risposta)comm).getRichiesta();
    }
    return emailDaRichiesta(em, r);
  }

}
