/*
 * Copyright (C) 2025 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.providers;

import it.virtualbit.legare.model.Commento;
import it.virtualbit.legare.model.Richiesta;
import it.virtualbit.legare.model.Risposta;
import java.text.DateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import org.openxava.jpa.XPersistence;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class ForumNotifier {
  public void send() {
    EntityManager em = XPersistence.createManager();
    try {      
      LinkedList<Commento> commenti = new LinkedList<>();
      
      TypedQuery<Richiesta> rq = em.createQuery("SELECT r FROM Richiesta r WHERE r.notificationsSentOn IS NULL OR r.notificationsSentOn < r.updatedAt", Richiesta.class);
      commenti.addAll((rq.getResultList()));
      
      TypedQuery<Risposta> aq = em.createQuery("SELECT r FROM Risposta r WHERE r.notificationsSentOn IS NULL OR r.notificationsSentOn < r.updatedAt", Risposta.class);
      commenti.addAll((aq.getResultList()));
      
      sendAll(em, commenti);

    } catch(Exception ex) {
      Logger.getLogger(BGScheduler.class.getName()).
              log(Level.SEVERE, null, ex);
      ex.printStackTrace();
    } finally {
      em.close();
    }
  } 

  private void sendAll(EntityManager em, LinkedList<Commento> commenti) {
    LinkedList<String> destinatari = new LinkedList<>();
    destinatari.addAll(Emailer.emailAmministratori(em));
    destinatari.addAll(Emailer.emailTecnici(em));
    HashMap<String, Collection<Commento>> pertinenze = new HashMap<>();
    for (Commento c: commenti) {
      EntityTransaction et = em.getTransaction();
      try {
        et.begin();
        c.setNotificationsSentTo("");
        et.commit();
      } finally {
        if (et.isActive())
          et.rollback();
      }
      Collection<String> emails = Emailer.emailDaCommento(em, c);
      destinatari.addAll(emails);
      for (String email: emails) {
        Collection<Commento> pertinenti = pertinenze.get(email);
        if (pertinenti == null)
          pertinenti = new LinkedList<>();
        pertinenti.add(c);
        pertinenze.put(email, pertinenti);
      }
    }
    
    destinatari.stream().distinct().forEach(
            (String dest) -> send(em, pertinenze.get(dest), dest));
  }

  private void send(EntityManager em, Collection<Commento> comm, String dest) {
    if (comm == null)
      return;
    
    StringBuilder testo = new StringBuilder();

    DateFormat df = DateFormat.getDateTimeInstance();
    comm.stream().sorted(
        (Commento c1, Commento c2) -> 
            c1 instanceof Richiesta ?
                c2 instanceof Richiesta ?
                    c1.getCreatedAt().compareTo(c2.getCreatedAt()) : -1 
            :   c2 instanceof Richiesta ?
                    1 : c1.getCreatedAt().compareTo(c2.getCreatedAt()))
               .forEach((Commento c) -> {
                  if (!c.getNotificationsSentTo().contains(dest)) {
                    Richiesta ric = null;
                    if (c instanceof Richiesta) {
                      ric = (Richiesta)c;
                      testo.append("- Nuova richiesta \"");
                    } else if (c instanceof Risposta) {
                      Risposta ris = (Risposta)c;
                      ric = ris.getRichiesta();
                      testo.append("- Nuovo commento per richiesta \"");
                    }
                    testo.append(ric.getOggetto());
                    testo.append("\", RPF ");                   
                    testo.append(ric.getIncarico().getRpf().getTestoScadenze());
                    testo.append(" in data/ora ");
                    testo.append(df.format(c.getCreatedAt()));
                    testo.append("\n");                     
                    testo.append("\n");
                  }
                });

    
    if (testo.length() == 0)
      return;
    
    testo.append("Accedere al portale di gestione gare per visualizzare gli interventi.\n\n");
    testo.append("Buona giornata.");
    
    String oggetto = "Nuovi interventi CiclatFM";
    String strTesto = "Buongiorno,\n" +
                 "\n" + 
                 "ci sono nuovi interventi per le seguenti richieste sulla piattaforma CiclatFM:\n" +
                 "\n" + testo.toString();
    EntityTransaction et = em.getTransaction();
    try {
      et.begin();
      
      for (Commento c: comm) {
        c.setNotificationsSentTo(c.getNotificationsSentTo() + " " + dest);
        c.setNotificationsSentOn(new Date());
      }
      if (!Emailer.send(dest, oggetto, strTesto))
        throw new Exception("Impossibile spedire notifica a " + dest + " con testo " + strTesto);
      
      et.commit();
    } catch(Exception ex) {
      Logger.getLogger(BGScheduler.class.getName()).
              log(Level.SEVERE, null, ex);
      ex.printStackTrace();
    } finally {
      if (et.isActive())
        et.rollback();
      em.close();
    }    
  }

}
