/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.providers;

import it.virtualbit.legare.actions.Scadenzabile;
import it.virtualbit.legare.actions.ScadenzeProvisioner;
import it.virtualbit.legare.model.*;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.*;
import org.openxava.jpa.XPersistence;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class BGScheduler {

  public void inviaAvvisi() {
    System.out.println("Invio avvisi di scadenza");
    EntityManager em = XPersistence.createManager();
    EntityTransaction et = em.getTransaction();
    try {
      et.begin();
      TypedQuery<Avviso> avq = em.createQuery("SELECT a FROM Avviso a WHERE DATE(a.dataAttivazione) <= :oggi AND DATE(a.dataAttivazione) >= :unasettimanafa AND a.deletedAt IS NULL AND (a.evaso IS NULL OR a.evaso = 0) ", Avviso.class);
      Date oggi = new Date();
      Date scorsaSettimana = new Date(oggi.getTime() - 86400L * 7 * 1000);
      avq.setParameter("oggi", oggi, TemporalType.DATE);
      avq.setParameter("unasettimanafa", scorsaSettimana, TemporalType.DATE);
      List<Avviso> alist = avq.getResultList();
      for (Avviso a: alist) {
        System.out.println("Invio email per avviso scadenza:" + a.getUuid());
        a.setOggetto(compilaCampi(em, a.getOggetto(), a));
        a.setTesto(compilaCampi(em, a.getTesto(), a));
        List<String> destinatari = trovaDestinatari(em, a);
        boolean atLeastOneSucceded = false;
    
        for (String dest: destinatari)
          atLeastOneSucceded |= Emailer.send(dest, a.getOggetto(), a.getTesto());
    
        a.setEvaso(atLeastOneSucceded);
        em.merge(a);
        et.commit();
      }
    }
    catch(Exception ex) {
      Logger.getLogger(BGScheduler.class.getName()).
              log(Level.SEVERE, null, ex);
      ex.printStackTrace();
    }
    finally {
      if (et.isActive())
        et.rollback();
      em.close();
    }
  }

  public void sincronizzaScadenze() {
    System.out.println("Sinc scadenze");
    EntityManager em = XPersistence.createManager();
    em.getEntityManagerFactory().getCache().evictAll();
    sincronizzaScadenzeRPF(em);
    sincronizzaScadenzeOPF(em);
  }

  private String compilaCampi(EntityManager em, String testo, Avviso a) {
    RPF r = null;
    if (a.getScadenza().getTipoDocumento() == Adempimento.ClasseRif.OPF) {
      OPF o = em.find(OPF.class, a.getScadenza().getIdDocumentoCollegato());
      r = o.getRpf();
    }
    if (a.getScadenza().getTipoDocumento() == Adempimento.ClasseRif.RPF) {
      r = em.find(RPF.class, a.getScadenza().getIdDocumentoCollegato());
    }
    if (r == null) // sono gestiti solo avvisi per scadenze relative a RPF o OPF
      throw new IllegalArgumentException("L'Avviso " + a.getUuid() + " non è supportato");

    LocalDate oggi = LocalDate.now();    
    int numGiorni = (int)(a.getScadenza().getIl().toEpochDay() - oggi.toEpochDay());
    
    return Emailer.compilaCampi(testo, r, oggi)
            .replaceAll("\\{\\{numeroGiorni\\}\\}", "" + numGiorni)
            ;
  }

  private void sincronizzaScadenzeRPF(EntityManager em) {
    
    TypedQuery<RPF> rpf = em.createQuery("SELECT r FROM RPF r WHERE DATE(r.dataFineServizi) > :oggi AND r.deletedAt IS NULL", RPF.class);
    rpf.setParameter("oggi", new Date(), TemporalType.DATE);
    List<RPF> rlist = rpf.getResultList();
    for (RPF r: rlist) {
      System.out.println("Sinc scadenze RPF :" + r.getTestoScadenze());
      sincronizzaScadenzabile(r, Adempimento.ClasseRif.RPF, em);
    }
  }

  private void sincronizzaScadenzeOPF(EntityManager em) {
    TypedQuery<OPF> opf = em.createQuery("SELECT o FROM OPF o, RPF r WHERE o.rpf = r AND o.deletedAt IS NULL AND r.deletedAt IS NULL AND DATE(r.dataFineServizi) > :oggi", OPF.class);
    opf.setParameter("oggi", new Date(), TemporalType.DATE);
    List<OPF> rlist = opf.getResultList();
    for (OPF o: rlist) {
      System.out.println("Sinc scadenze OPF :" + o.getTestoScadenze());
      sincronizzaScadenzabile(o, Adempimento.ClasseRif.OPF, em);
    }
  }

  private void sincronizzaScadenzabile(Scadenzabile _r, Adempimento.ClasseRif rif, EntityManager em) {
    Scadenzabile r = em.find(_r.getClass(), _r.getUuid());
    em.refresh(r);
    TypedQuery<Scadenza> qscad = em.createQuery("SELECT s FROM Scadenza s WHERE s.tipoDocumento =:rif AND s.idDocumentoCollegato=:iddoc AND deletedAt IS NULL", Scadenza.class);
    qscad.setParameter("rif", rif);
    qscad.setParameter("iddoc", r.getUuid());
    List<Scadenza> esistenti = qscad.getResultList();
    System.out.println(esistenti.size() + " scadenza/e " + rif.toString() + " esistente/i.");
    ScadenzeProvisioner sp = new ScadenzeProvisioner();
    List<Scadenza> necessarie = sp.crea(r);
    System.out.println(necessarie.size() + " scadenza/e " + rif.toString() + " necessaria/i.");
    List<Scadenza> daEliminare = new LinkedList<>();
    System.out.println(daEliminare.size() + " scadenza/e " + rif.toString() + " da eliminare.");
    List<Scadenza> daCreare = new LinkedList<>();
    System.out.println(daCreare.size() + " scadenza/e " + rif.toString() + " da creare.");
    
    Comparator<Scadenza> cmp = (s1, s2) -> s1.getIl().compareTo(s2.getIl());
    esistenti.sort(cmp);
    necessarie.sort(cmp);
    for (Scadenza e: esistenti) {
      if (Collections.binarySearch(necessarie, e, cmp) < 0) // se la scadenza esistente non è più necessaria
        daEliminare.add(e);
    }
    
    for (Scadenza n: necessarie) {
      if (Collections.binarySearch(esistenti, n, cmp) < 0) // se la scadenza necessaria non esiste ancora
        daCreare.add(n);
    }
    
    for (Scadenza e: daEliminare) {
      EntityTransaction et = em.getTransaction();
      try {
        et.begin();
        for (Avviso a: e.getAvvisi()) {
            a.setDeletedAt(new java.util.Date());
            em.merge(a);
        }
        e.setDeletedAt(new java.util.Date());
        em.merge(e);
        et.commit();
      } catch (Exception ex) { 
      } finally {
        if (et.isActive())
          et.rollback();
      }
    }
    
    sp.salva(daCreare);
  }

  private List<String> trovaDestinatari(EntityManager em, Avviso a) {
    LinkedList<String> result = new LinkedList<>();
    
    // Al momento per qualsiasi tipo di azione necessaria notifico sempre
    // gli stessi account
    switch (a.getAzione()) {
      case CARICAMENTO_PDA:
      case CARICAMENTO_POA:
      case CARICAMENTO_VdC:
      case CARICAMENTO_Livelli_Di_Servizio:
        result.addAll(Emailer.emailAmministratori(em));
        result.addAll(Emailer.emailTecnici(em));
        break;
    }
    return result;
  }
}