/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.providers;

import com.openxava.naviox.impl.SignInHelperProvider;
import it.virtualbit.legare.model.*;
import it.virtualbit.legare.model.redis.ACL;
import it.virtualbit.legare.model.redis.Connection;
import it.virtualbit.legare.model.redis.Role;
import java.util.List;
import javax.persistence.TypedQuery;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.EnumUtils;
import org.openxava.jpa.XPersistence;
import org.openxava.util.Messages;
import org.redisson.api.RLock;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class SignInHelper extends SignInHelperProvider {

  @Override
  public boolean isAuthorized(ServletRequest request, String userName,
                              String password, Messages errors,
                              String unauthorizedMessage) {
    try {
      TypedQuery<Account> a = XPersistence.getManager().createQuery(
              "SELECT a FROM Account a WHERE a.nomeUtente=:nome AND a.password=:password", Account.class);
      a.setParameter("nome", userName);
      a.setParameter("password", password);
      Account acc = a.getSingleResult();
      if (acc == null) {
        return super.isAuthorized(request, userName, password, errors,
                                "Nome utente e/o password non corretti");
      } else {
        return true;
      }
    } catch (Exception ex) {
        return super.isAuthorized(request, userName, password, errors,
                                "Nome utente e/o password non corretti");
    }
  }
  
  
  @Override
  public void signIn(HttpServletRequest request, String userName) {
    super.signIn(request, userName);
    loadRoles(userName);
  }
  
  private void loadRoles(String userName) {
    initACL(userName, cacheRoles(userName));
  }
  
  private String cacheRoles(String cuser) {
    RLock lock = Connection.get().getLock("cacheRoles");
    String group = "none";
    if (lock.tryLock()) {
      try {
          TypedQuery<Account> a = XPersistence.getManager().createQuery("SELECT a FROM Account a WHERE a.nomeUtente=:nome", Account.class);
          a.setParameter("nome", cuser);
          List<Account> accs = a.getResultList();
          if (accs.isEmpty()) {
            return group;
          }
          Account acc = accs.getFirst();
          group = acc.getGruppo();
          String sruoli = acc.getRuoli();
          String[] ruoli = sruoli.split(",");
          for (String r: ruoli) {
            if (EnumUtils.isValidEnumIgnoreCase(Role.class, r))
              EnumUtils.getEnumIgnoreCase(Role.class, r).add(cuser, group);
          }
          
          SCL.findAdmin();
      } catch (Exception e) {
        e.printStackTrace();
      }
      finally {
        lock.unlock();
      }
    }
    return group;
  }
  
  private void initACL(String cuser, String cgroup) {
    if (!Role.ADMIN.includesCurrentUser() && !Role.TECNICO.includesCurrentUser()) {
      if (Role.ENTE.includesCurrentUser()) {
        TypedQuery<Ente> e = XPersistence.getManager().createQuery("SELECT e FROM Ente e WHERE (e.ownerUsername=:nome  OR e.ownerGroup=:gruppo) AND deletedAt IS NULL", Ente.class);
        e.setParameter("nome", cuser);
        e.setParameter("gruppo", cgroup);
        List<Ente> entelist = e.getResultList();
        if (entelist.isEmpty()) {
          throw new IllegalStateException("Utenza non ancora abilitata.");
        }
        Ente ente = entelist.getFirst();
        ACL.addReadPermission(cuser, ente);

        TypedQuery<RPF> rpf = XPersistence.getManager().createQuery("SELECT r FROM RPF r WHERE r.ente=:ente AND deletedAt IS NULL", RPF.class);
        rpf.setParameter("ente", ente);
        List<RPF> rlist = rpf.getResultList();
        for (RPF r: rlist) {
          ACL.addReadPermission(cuser, r);
          ACL.addReadPermission(cuser, r.getLotto());
          ACL.addReadPermission(cuser, r.retrieveCommessa());
        }           

        TypedQuery<Incarico> incq = XPersistence.getManager().createQuery("SELECT i FROM Incarico i WHERE i.rpf IN (:rpflist) AND deletedAt IS NULL", Incarico.class);
        incq.setParameter("rpflist", rlist);
        List<Incarico> ilist = incq.getResultList();
        for (Incarico i: ilist) {
          ACL.addReadPermission(cuser, i);
          ACL.addReadPermission(cuser, i.getEsecutori());
        }         

      }
      if (Role.ESECUTORE.includesCurrentUser()) {
        TypedQuery<EsecutoriDelServizio> eds = XPersistence.getManager().createQuery("SELECT e FROM EsecutoriDelServizio e WHERE (e.ownerUsername=:nome OR e.ownerGroup=:gruppo) AND deletedAt IS NULL", EsecutoriDelServizio.class);
        eds.setParameter("nome", cuser);
        eds.setParameter("gruppo", cgroup);
        List<EsecutoriDelServizio> elst = eds.getResultList();
        if (elst.isEmpty()) {
          throw new IllegalStateException("Utenza non ancora abilitata.");
        }
        EsecutoriDelServizio e = eds.getSingleResult();
        ACL.addReadPermission(cuser, e);

        TypedQuery<Incarico> incq = XPersistence.getManager().createQuery("SELECT i FROM Incarico i WHERE i.esecutori=:eds AND deletedAt IS NULL", Incarico.class);
        incq.setParameter("eds", e);
        List<Incarico> ilist = incq.getResultList();
        for (Incarico i: ilist) {
          ACL.addReadPermission(cuser, i);
          RPF rpf = i.getRpf();
          if (rpf != null) {
            ACL.addReadPermission(cuser, rpf);
            ACL.addReadPermission(cuser, rpf.getEnte());
            ACL.addReadPermission(cuser, rpf.retrieveCommessa());
          }
        } 
      }
    } 
  }
}
