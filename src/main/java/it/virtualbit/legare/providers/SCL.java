/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.providers;

import it.virtualbit.legare.model.Account;
import it.virtualbit.legare.model.redis.Role;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.persistence.TypedQuery;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.openxava.jpa.XPersistence;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
@WebListener
public class SCL implements ServletContextListener {

    private ScheduledExecutorService scheduler;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        System.out.println("Legare Context initilized");
        findAdmin();
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(() -> new BGScheduler().sincronizzaScadenze(), 1, 1, TimeUnit.MINUTES);
        scheduler.scheduleAtFixedRate(() -> new BGScheduler().inviaAvvisi(), 2, 720, TimeUnit.MINUTES);
        scheduler.scheduleAtFixedRate(() -> new ForumNotifier().send(), 10, 10, TimeUnit.MINUTES);
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        scheduler.shutdownNow();
    }
    
    public static void findAdmin() {
      try {
        TypedQuery<Account> a = XPersistence.getManager().createQuery("SELECT a FROM Account a WHERE a.ruoli LIKE CONCAT('%', :admrole, '%')", Account.class);
        a.setParameter("admrole", Role.ADMIN.label);
        a.setMaxResults(1);
        Account acc = a.getSingleResult();
        Role.ADMIN.add(acc.getNomeUtente(), acc.getGruppo());      
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
}