/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.*;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = BaseEntity.baseFilter,
     filter = PDA.Filter.class
     )
public class PDA extends BaseEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  public static class Filter extends BaseEntity.AbstractFilter<PDA> {
    @Override
    protected Class<PDA> getEntityClass() {
      return PDA.class;
    }
  }

  @Column
  @Required
  LocalDate dataProtocollo;
    
  @Column
  LocalDate dataSopralluogo;

  @Column
  LocalDate dataPresentazione;

  @Column
  LocalDate dataApprovazione;

  @Files
  @Column(length=32)
  private String allegati;

  @Column(length = 30)
  @Required
  String codice;

  @Column(length = 250)
  String descrizione;
  
  @ManyToOne( 
    fetch=FetchType.LAZY, 
    optional=true)
  @DescriptionsList(descriptionProperties="codice")
  OPF opf;
}