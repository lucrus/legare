/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.*;
import org.openxava.filters.FilterException;
import org.openxava.filters.IFilter;
import org.openxava.util.Users;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = "${nomeUtente} LIKE ?",
     filter = Account.Filter.class)
public class Account extends UUIDEntity implements Serializable {
  
  public static class Filter implements IFilter {

    @Override
    public Object filter(Object o) throws FilterException {
      String cu = Users.getCurrent();
      Object[] res = {cu};
      return res;
    }
    
  }

  @Column(length = 100)
  @Required
  @ReadOnly
  String nomeUtente;

  @Column(length = 100)
  @Required
  @ReadOnly
  String gruppo;

  @Column(length = 100)
  @Required
  String nome;

  @Column(length = 100)
  @Required
  String cognome;

  @Column(length = 250)
  String descrizione;

  @Column(length = 250)
  @Required
  @ReadOnly
  String email;

  @Column(length = 250)
  String telefono;
  
  @Column(length = 250)
  @Hidden
  @ReadOnly
  String ruoli;

  @Column(length = 250)
  @Hidden
  @ReadOnly
  String password;
}