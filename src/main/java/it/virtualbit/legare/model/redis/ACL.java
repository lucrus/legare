/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model.redis;

import it.virtualbit.legare.model.UUIDEntity;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class ACL {
  private static final String aclPrefix = "acl_for_";
  
  public static void addReadPermission(String user, UUIDEntity entity) {
    if (entity == null)
      return;
    RedisMap aclMap = new RedisMap(aclPrefix + user);
    String className = entity.getClass().getSimpleName();
    int idx = className.indexOf('$');
    if (idx > 0)
      className = className.substring(0, idx);
    aclMap.add(className, entity.getUuid());
  }
  
  public static String[] getCurrentUserReadPermissions(Class<? extends UUIDEntity> clazz) {
    return getReadPermissions(Role.getCurrentUsername(), clazz);
  }
  
  public static String[] getReadPermissions(String user, Class<? extends UUIDEntity> clazz) {
    String className = clazz.getSimpleName();
    RedisMap aclMap = new RedisMap(aclPrefix + user);
    return aclMap.getAll().get(className);
  }
}
