/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model.redis;

import java.util.Arrays;
import java.util.LinkedList;
import org.redisson.api.RMap;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public class RedisMap {

  private String mapKey;
  
  public RedisMap(String key) {
      mapKey = key;
  }

  public RMap<String, String[]> getAll() {
    return Connection.get().getMap(mapKey);
  }

  public void add(String key, String value) {
    RMap<String, String[]> all = getAll();
    String[] aCurrent = all.get(key);
    if (aCurrent == null)
      aCurrent = new String[0];
    LinkedList<String> current = new LinkedList<>(Arrays.asList(aCurrent));
    if (!current.contains(value)) {
      current.add(value);
      aCurrent = current.toArray(new String[current.size()]);
      all.put(key, aCurrent);
    }
  }
}
