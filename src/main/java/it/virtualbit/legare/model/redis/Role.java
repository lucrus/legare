/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model.redis;

import java.util.Arrays;
import java.util.stream.Stream;
import org.openxava.util.Users;
import org.redisson.api.RMap;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
public enum Role {
  ADMIN("Admin"),
  TECNICO("Tecnico"),
  ESECUTORE("Esecutore"),
  ENTE("Ente");  
    
  public final String label;

  private static final RedisMap usersRolesMap = new RedisMap("usersroles");
  private static final RedisMap rolesUsersMap = new RedisMap("rolesusers");
  private static final RedisMap usersGroupMap = new RedisMap("usersgroup");
  
  private Role(String label) {
      this.label = label;
  }

  @Override 
  public String toString() {
    return label;
  }
  
  public boolean includesCurrentUser() {
    return userHasAnyRoleOf(new String[]{label});
  }
  
  public static RMap<String, String[]> getAll() {
    return usersRolesMap.getAll();
  }

  public static boolean userHasAnyRoleOf(String[] wanted) {
    final String cuser = getCurrentUsername();
    if (!getAll().containsKey(cuser)) {
      return false;
    } 
    String[] roles = getAll().get(cuser);
    Stream rStream = Arrays.stream(roles); 
    Stream wStream = Arrays.stream(wanted); 
    boolean result = rStream.anyMatch(
            r -> wStream.anyMatch(
                      w -> w.toString().toLowerCase().equals(r.toString().toLowerCase())));
    return result;
  }
  
  public void add(String userName, String group) {
    usersRolesMap.add(userName, label);
    rolesUsersMap.add(label, userName);
    if (group != null)
      usersGroupMap.add(userName, group);
  }

  public static String getCurrentUsername() {
    String result = Users.getCurrent();
    if (result == null) {
      String[] admins = rolesUsersMap.getAll().get(Role.ADMIN.label);
      if (admins.length > 0) {
        result = admins[0];
      }
    }
    return result;
  }

  public static String getCurrentUsergroup() {
    String user = getCurrentUsername();
    String[] groups = usersGroupMap.getAll().get(user);
    for (String g: groups) {
      if (g != null)
        return g;
    }
    return null;
  }
}
