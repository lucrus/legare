/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import it.virtualbit.legare.actions.Scadenzabile;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.*;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = BaseEntity.baseFilter,
     filter = RPF.Filter.class
     )
public class RPF extends BaseEntity implements Serializable, Scadenzabile {
  private static final long serialVersionUID = 1L;

  public static class Filter extends BaseEntity.AbstractFilter<RPF> {
    @Override
    protected Class<RPF> getEntityClass() {
      return RPF.class;
    }
  }
  
  @Column(length = 30)
  @Required
  String codice;

  @Column(length = 250)
  String descrizione;
  
  @ManyToOne( 
    fetch=FetchType.LAZY, 
    optional=false)
  @DescriptionsList(descriptionProperties="codiceConCommessa")
  Lotto lotto;

  @ManyToOne( 
    fetch=FetchType.LAZY, 
    optional=false)
  @DescriptionsList(descriptionProperties="nome")
  Ente ente;

  @Column
  @Required
  LocalDate dataProtocollo;
    
  @Column
  @Required
  LocalDate dataInizioServizi;

  @Column
  @Required
  LocalDate dataFineServizi;
  
  @Column
  String codiceFiscalePA;
  
  @Column
  String indirizzoPA;
  
  @Column 
  String puntoOrdinante;
  
  @Column
  String codiceFiscalePuntoOrdinante;
  
  @Column
  String telefonoReferentePA;
  
  @Column 
  String numeroOrdine;
  
  @Column
  LocalDate dataInvioRPF;
  
  @Column
  LocalDate dataAccettazioneRPF;
  
  @Column
  LocalDate dataComunicazioneValiditàFormale;
  
  @Column
  LocalDate dataSopralluogo;
  
  @Column
  LocalDate dataInvioProgettoAssorbimento;
  
  @OneToMany(mappedBy = "rpf")
  Collection<Incarico> incarichi;  
  
  @OneToMany(mappedBy = "rpf")
  Collection<Edificio> edifici;
  @Files
  @Column(length=32)
  private String allegati;

  @TextArea
  @Column(length = 1000)
  String note;

  @Override
  @Hidden
  public LocalDate getDataIniziale() {
    return this.getDataProtocollo();
  }
  
  @Override
  @Hidden
  public LocalDate getDataFinale() {
    return this.getDataFineServizi();
  }

  @Override
  @Hidden
  public String getTestoScadenze() {
    return "RPF " + codice + " / " + retrieveCommessa().getPortale().toString() + " / " + retrieveCommessa().getNome() + " / " + ente.getNome();
  }

  @Override
  @Hidden
  public Commessa retrieveCommessa() {
    return getLotto().getCommessa();
  }
}