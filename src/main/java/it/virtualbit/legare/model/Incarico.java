/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.*;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = BaseEntity.baseFilter,
     filter = Incarico.Filter.class
     )
public class Incarico extends BaseEntity implements Serializable {

  public static class Filter extends BaseEntity.AbstractFilter<Incarico> {
    @Override
    protected Class<Incarico> getEntityClass() {
      return Incarico.class;
    }    
  }

  private static final long serialVersionUID = 1L;

  @Column
  String descrizione;

  @ManyToOne( 
    fetch=FetchType.LAZY, 
    optional=false)
  @DescriptionsList(descriptionProperties="nome")
  EsecutoriDelServizio esecutori;

  @ManyToOne( 
    fetch=FetchType.LAZY, 
    optional=false)
  @DescriptionsList(descriptionProperties="nome")
  Servizio servizio;

  @Column
  @DisplaySize(18)
  BigDecimal importo;
  
  @ManyToOne( 
    fetch=FetchType.LAZY, 
    optional=false)
  @DescriptionsList(descriptionProperties="codice")
  RPF rpf;
  
  @TextArea
  @Column(length = 1000)
  String note;

  @Files
  @Column(length=32)
  private String allegati;
  
  public String getRPFConServizio() {
    return rpf.getCodice() + " - " + 
            rpf.dataInizioServizi.format(DateTimeFormatter.ISO_DATE) + " - " + 
            servizio.getNome() + " (" + descrizione + ")";
  }
  
  @OneToMany(mappedBy = "incarico")
  protected Collection<Richiesta> richieste;
  
}