/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.DescriptionsList;
import org.openxava.annotations.ReadOnly;
import org.openxava.annotations.Tab;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = "${deletedAt} is null")
public class Risposta extends Commento implements Serializable {
  private static final long serialVersionUID = 1L;

  String getDataRisposta() {
    Date d = createdAt != null ? createdAt : new Date();
    LocalDate ld = d.toInstant().atZone(ZoneId.systemDefault())
      .toLocalDate();
    return ld.format(DateTimeFormatter.ISO_DATE);
  }
  
  @ReadOnly
  @ManyToOne(
    fetch=FetchType.LAZY, 
    optional=false)
  @DescriptionsList(descriptionProperties="risposte")
  Richiesta richiesta;
    
}