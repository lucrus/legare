/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import it.virtualbit.legare.model.redis.ACL;
import it.virtualbit.legare.model.redis.Role;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.ValidationException;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.Hidden;
import org.openxava.annotations.ReadOnly;
import org.openxava.filters.FilterException;
import org.openxava.filters.IFilter;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity extends UUIDEntity implements Serializable {

  private static final long serialVersionUID = 1L;
  
  public static final String baseFilter = "${deletedAt} is null and (${uuid} IN (?) or ${hidden} = ? or ${ownerUsername} = ? or ${ownerGroup} = ?)";
      
  public static abstract class AbstractFilter<T extends UUIDEntity> implements IFilter {
    @Override
    public Object filter(Object o) throws FilterException {
      boolean alwaysShow = Role.ADMIN.includesCurrentUser() || Role.TECNICO.includesCurrentUser();
      Class<T> eClass = getEntityClass();
      String[] readPerms = ACL.getCurrentUserReadPermissions(eClass); 
      if (readPerms == null)
        readPerms = new String[]{"no_id"};
      Collection cReadPerms = Arrays.asList(readPerms);
      Object[] result = new Object[]{cReadPerms, Boolean.valueOf(!alwaysShow), Role.getCurrentUsername(), Role.getCurrentUsergroup()};
      return result;
    }
    
    protected abstract Class<T> getEntityClass();
  }

  @Column
  @Hidden
  String ownerUsername;
  
  @Column
  @Hidden
  String ownerGroup;

  @Column
  @Hidden
  boolean readonly = false;
  
  @Column
  @Hidden
  @ReadOnly
  boolean hidden = false;

  @Column
  @Hidden
  @Temporal(value = TemporalType.TIMESTAMP)
  Date deletedAt;

  @Column
  @Hidden
  @Temporal(value = TemporalType.TIMESTAMP)
  Date createdAt;

  @Column
  @Hidden
  @Temporal(value = TemporalType.TIMESTAMP)
  Date updatedAt;

  public BaseEntity()
  {
    super();
  }

  public BaseEntity(String uuid)
  {
    super(uuid);
  }

  String[] getRolesWithCreatePermissionOn() {
    String[] result = { Role.ADMIN.label, Role.TECNICO.label };
    return result;
  }

  String[] getRolesWithDeletePermission() {
    String[] result = { Role.ADMIN.label, Role.TECNICO.label };
    return result;
  }

  String[] getRolesWithUpdatePermission() {
    String[] result = { Role.ADMIN.label, Role.TECNICO.label };
    return result;
  }
  
  @Override
  void onPrePersist() throws Exception 
  {
    super.onPrePersist();
    checkCreatePerms();
  }
  
  @Override
  public void initAutomaticColumns() {
    super.initAutomaticColumns();
    updatedAt = new Date();
    if (createdAt == null)
      createdAt = updatedAt;
    if (ownerUsername == null) {
      ownerUsername = Role.getCurrentUsername();
    }
    if (ownerGroup == null) {
      ownerGroup = Role.getCurrentUsergroup();
    }
  }

  @PreUpdate
  void onPreUpdate() throws Exception 
  {
    checkUpdatePerms();
  }

  @PreRemove
  void onPreRemove() throws Exception 
  {
    checkDeletePerms();
  }
  
  private void checkCreatePerms() {
    if (!Role.userHasAnyRoleOf(getRolesWithCreatePermissionOn())) {
      throw new ValidationException("Non hai il permesso di modificare questo dato");      
    }
  }   

  private void checkUpdatePerms() {
    if (readonly) {
      throw new ValidationException("Questo oggetto non è più modificabile");      
    }
    final String cuser = Role.getCurrentUsername();
    if (cuser.equals(ownerUsername)) // il proprietario del record è sempre autorizzato
      return;
    if (!Role.userHasAnyRoleOf(getRolesWithUpdatePermission())) {
      throw new ValidationException("Non hai il permesso di modificare questo dato");      
    }
  }   

  private void checkDeletePerms() {
    if (readonly) {
      throw new ValidationException("Questo oggetto non è più eliminabile");      
    }
    final String cuser = Role.getCurrentUsername();
    if (cuser.equals(ownerUsername)) // il proprietario del record è sempre autorizzato
      return;
    if (!Role.userHasAnyRoleOf(getRolesWithDeletePermission())) {
      throw new ValidationException("Non hai il permesso di eliminare questo dato");      
    }
  }
}
