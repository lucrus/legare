/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.Hidden;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@MappedSuperclass
@Getter
@Setter
public abstract class UUIDEntity implements Serializable {

  private static final long serialVersionUID = 1L;
    
  @Id @Hidden
  @Basic(optional = false, fetch = FetchType.EAGER)
  @Column(name = "uuid")
  private String uuid;

  public UUIDEntity()
  {
  }

  public UUIDEntity(String uuid)
  {
    this.uuid = uuid;
  }
        
  @Override
  public int hashCode()
  {
    int hash = 0;
    hash += (uuid != null ? uuid.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object)
  {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof UUIDEntity))
    {
      return false;
    }
    UUIDEntity other = (UUIDEntity) object;
    return !((this.uuid == null && other.getUuid() != null) || (this.uuid != null && !this.uuid.equals(other.getUuid())));
  }

  @Override
  public String toString()
  {
    return getClass().getName() + "[ uuid=" + uuid + " ]";
  }

  @PrePersist
  void onPrePersist() throws Exception 
  {
    initAutomaticColumns();
  }
  
  public void initAutomaticColumns() {
    if (uuid == null)
      uuid = UUID.randomUUID().toString();
  }
}
