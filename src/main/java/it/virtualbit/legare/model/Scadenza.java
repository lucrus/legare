/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import it.virtualbit.legare.model.redis.Role;
import java.io.Serializable;
import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.Collection;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.*;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 * @param <T>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = BaseEntity.baseFilter,
     filter = Scadenza.Filter.class
     )
public class Scadenza extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  public static class Filter extends BaseEntity.AbstractFilter<Scadenza> {
    @Override
    protected Class<Scadenza> getEntityClass() {
      return Scadenza.class;
    }    
  }

  @Column
  @ReadOnly
  String descrizione;
  
  @ReadOnly
  @Column
  String impegno;
    
  @Stereotype("HIGHLIGHTER_BYURGENZA")
  public String getUrgenza() {
    if (evasa) return "5 - evasa";
    LocalDate oggi = LocalDate.now();
    long daysBetween = DAYS.between(oggi, il);
    if (daysBetween < 0) return "0 - Scaduta";
    if (daysBetween == 0) return "1 - Urgentissima (oggi)";
    if (daysBetween == 1) return "2 - Urgente (domani)";
    if (daysBetween < 4) return "3 - Imminente (tre giorni)";
    return "4 - Normale (più di 3 giorni)";
  }

  @ReadOnly
  @ManyToOne( 
    fetch=FetchType.LAZY, 
    optional=true)
  Adempimento adempimento;
  
  @Column
  @OneToMany(mappedBy = "scadenza")
  Collection<Avviso> avvisi;
  
  @Column
  @Hidden
  String idDocumentoCollegato;
  
  @Column
  Adempimento.ClasseRif tipoDocumento;
  
  @Files
  @Column(length=32)
  private String allegati;
  
  @Column
  @Required
  LocalDate il;
  
  @Column
  @Required
  boolean evasa;
  
  @TextArea
  @Column(length = 1000)
  String note;
  
  @Override
  String[] getRolesWithCreatePermissionOn() {
    String[] result = { Role.ADMIN.label };
    return result;
  }

  @Override
  String[] getRolesWithDeletePermission() {
    String[] result = { Role.ADMIN.label };
    return result;
  }

  @Override
  String[] getRolesWithUpdatePermission() {
    String[] result = { Role.ADMIN.label };
    return result;
  }  
    
}