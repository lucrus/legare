/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import it.virtualbit.legare.model.redis.Role;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.DescriptionsList;
import org.openxava.annotations.Required;
import org.openxava.annotations.Tab;

/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = "${deletedAt} is null")
public class Adempimento extends BaseEntity implements Serializable {
  private static final long serialVersionUID = 1L;
    
  @Column
  @Required
  String nome;
    
  @Column
  ClasseRif relativoA;
  public static enum ClasseRif { RPF, OPF }

  @Column
  private Origine calcolo;
  public static enum Origine { GIORNO_DEL_MESE, NUMERO_DI_GIORNI }
  
  @Column
  private Ricorrenza ricorrenza;
  public static enum Ricorrenza { UNA_VOLTA, OGNI_MESE }

  @Column
  int giorni; // giorni entro cui portare a termine l'adempimento, a partire dalla data scelta dell'impegno
  
  @Column
  String dataDiRiferimento;

  @OneToMany(mappedBy = "adempimento")
  Collection<ModelloAvvisoScadenza> avvisi;
    
  @Override
  String[] getRolesWithCreatePermissionOn() {
    String[] result = { Role.ADMIN.label };
    return result;
  }

  @Override
  String[] getRolesWithDeletePermission() {
    String[] result = { Role.ADMIN.label };
    return result;
  }

  @Override
  String[] getRolesWithUpdatePermission() {
    String[] result = { Role.ADMIN.label };
    return result;
  }
  
  @ManyToOne( 
    fetch=FetchType.LAZY, 
    optional=true)
  @DescriptionsList(descriptionProperties="nome")
  Commessa commessa;
  
  
}
