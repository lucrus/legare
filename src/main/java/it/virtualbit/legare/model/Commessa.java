/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.*;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = BaseEntity.baseFilter,
     filter = Commessa.Filter.class
     )
public class Commessa extends BaseEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  public static class Filter extends BaseEntity.AbstractFilter<Commessa> {
    @Override
    protected Class<Commessa> getEntityClass() {
      return Commessa.class;
    }    
  }
  
  
  @Column
  private PortaleGare portale;
  public enum PortaleGare { CONSIP, IntercentER };

  @Column(length = 30)
  @Required
  String nome;

  @Column(length = 250)
  String descrizione;

  @Column
  @Required
  LocalDate dataInizio;
  
  @Column
  @Required
  LocalDate dataLimiteContrattualizzazione;
  
  @OneToMany(mappedBy = "commessa")
  Collection<Lotto> lotti;

  @Files
  @Column(length=32)
  private String allegati;

  @TextArea
  @Column(length = 1000)
  String note;
}