/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.*;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = BaseEntity.baseFilter,
     filter = Richiesta.Filter.class)
public class Richiesta extends Commento implements Serializable {
  private static final long serialVersionUID = 1L;
  
  public static class Filter extends BaseEntity.AbstractFilter<Richiesta> {
    @Override
    protected Class<Richiesta> getEntityClass() {
      return Richiesta.class;
    }    
  }

  @Column(length = 100)
  @Required
  String oggetto;
      
  @ReadOnly
  @ManyToOne(
    fetch=FetchType.LAZY, 
    optional=false)
  @DescriptionsList(descriptionProperties="RPFConServizio")
  Incarico incarico;

  @Column
  private Stato stato;
  public enum Stato { APERTA, RISOLTA, IRRISOLVIBILE };
  
  @OneToMany(mappedBy = "richiesta")
  Collection<Risposta> risposte;

}