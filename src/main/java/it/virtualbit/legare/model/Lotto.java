/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import java.io.Serializable;
import java.time.format.DateTimeFormatter;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.*;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = BaseEntity.baseFilter,
     filter = Lotto.Filter.class
     )
public class Lotto extends BaseEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  public static class Filter extends BaseEntity.AbstractFilter<Lotto> {
    @Override
    protected Class<Lotto> getEntityClass() {
      return Lotto.class;
    }    
  }

  @Column(length = 30)
  @Required
  String codice;

  @Column(length = 250)
  String descrizione;

  @ManyToOne(fetch = FetchType.EAGER)
  @DescriptionsList(descriptionProperties = "nome")
  private Commessa commessa;  

  @TextArea
  @Column(length = 1000)
  String note;
  
  public String getCodiceConCommessa() {
    return codice + " - " + getCommessa().getNome() + " - " + getCommessa().getDataInizio().format(DateTimeFormatter.ISO_DATE);
  }
}