/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import it.virtualbit.legare.actions.Scadenzabile;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.*;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = BaseEntity.baseFilter,
     filter = OPF.Filter.class
     )
public class OPF extends BaseEntity implements Serializable, Scadenzabile {
  private static final long serialVersionUID = 1L;

  public static class Filter extends BaseEntity.AbstractFilter<OPF> {
    @Override
    protected Class<OPF> getEntityClass() {
      return OPF.class;
    }    
  }

  @Column
  @Required
  LocalDate dataProtocollo;

  @Column
  LocalDate dataStipula;
  
  @Column
  LocalDate dataAccettazione;

  @Column
  LocalDate dataInizioServizi;

  @Column
  LocalDate dataFineServizi;
  
  @Column
  @DisplaySize(18)
  BigDecimal importoContratto;

  @Column
  @Required
  LocalDate dataTermine;

  @Column(length = 30)
  @Required
  String codice;

  @Column(length = 250)
  String descrizione;

  @ManyToOne( 
    fetch=FetchType.LAZY, 
    optional=true)
  @DescriptionsList(descriptionProperties="codice")
  RPF rpf;

  @Files
  @Column(length=32)
  private String allegati;

  @Files
  @Column(length=32)
  private String livelliDiServizio;
  
  @TextArea
  @Column(length = 1000)
  String note;

  @Hidden
  @Override
  public LocalDate getDataFinale() {
    return rpf.getDataFinale();
  }

  @Hidden
  @Override
  public LocalDate getDataIniziale() {
    return getDataAccettazione();
  }

  @Hidden
  @Override
  public String getTestoScadenze() {
    return "OPF " + codice + " / " + rpf.getTestoScadenze();
  }
 
  @Hidden
  @Override
  public Commessa retrieveCommessa() {
    if (rpf != null)
      return rpf.retrieveCommessa();
    return null;
  }
}