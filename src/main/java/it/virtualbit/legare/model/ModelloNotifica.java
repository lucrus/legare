/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.TextArea;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@Getter
@Setter
@MappedSuperclass
public class ModelloNotifica extends BaseEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(length = 200)
  String oggetto;
  
  @TextArea
  @Column(length = 5000)
  String testo;

  @TextArea
  @Column(length = 1000)
  String noteAdUsoInterno;

}