/*
 * Copyright (C) 2024 Lucio Crusca <nfo@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.*;


/**
 *
 * @author Lucio Crusca <nfo@virtualbit.it>
 */
@Entity
@Getter
@Setter
@Tab(baseCondition = "${deletedAt} is null")
public class Edificio extends BaseEntity implements Serializable {
  private static final long serialVersionUID = 1L;
    
  @Column
  @Required
  String nome;
    
  @Column
  String indirizzo;
  
  @Column
  String cap;
  
  @Column
  String comune;
  
  @Column
  Boolean anagraficaTecnicaRichiesta;

  @ManyToOne( 
    fetch=FetchType.LAZY, 
    optional=false)
  @DescriptionsList(descriptionProperties="codice")
  RPF rpf;     
  
  @Files
  @Column(length=32)
  private String pianoOperativoAttivita;
  
  @Files
  @Column(length=32)
  private String verbaliDiControllo;

  @OneToMany(mappedBy = "edificio")
  Collection<AnagraficaTecnica> pianiEdAnagraficheTecniche;

}
