/*
 * Copyright © 2024 Lucio Crusca <info@virtualbit.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.virtualbit.legare.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.openxava.annotations.Files;
import org.openxava.annotations.Hidden;
import org.openxava.annotations.TextArea;

/**
 *
 * @author Lucio Crusca <info@virtualbit.it>
 */
@MappedSuperclass
@Getter
@Setter
public abstract class Commento extends BaseEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  @TextArea
  @Column(length = 2000)
  String dettagli;
   
  String getAutore() {
    return super.ownerUsername;
  }
 
  @Files
  @Column(length=32)
  private String allegati;
  
  @Column(length=5000)
  @Hidden
  String notificationsSentTo;
  
  @Column
  @Hidden
  @Temporal(value = TemporalType.TIMESTAMP)
  Date notificationsSentOn;
  
}