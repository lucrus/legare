#!/bin/bash

MYDIR=`dirname $0`
MYDIR=`realpath $MYDIR`
STRIP='/src/main/resources'
CUTCHARS=${#STRIP}
TOTCHARS=${#MYDIR}
KEEPCHARS=$((TOTCHARS - CUTCHARS))
PROJECTDIR=${MYDIR:0:KEEPCHARS}
cd $PROJECTDIR
mvn clean package
RESULT=$?
if [ $RESULT -eq 0 ] ; then
  ssh legare.virtualbit.it systemctl stop tomcat
  ssh legare.virtualbit.it rm -rf '/opt/tomcat/tomcat/webapps/legare*'
  scp target/legare.war legare.virtualbit.it:/opt/tomcat/tomcat/webapps/
  ssh legare.virtualbit.it systemctl start tomcat
fi

