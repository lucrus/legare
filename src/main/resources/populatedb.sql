USE legaredb;

INSERT INTO `Account` (`uuid`, `cognome`, `descrizione`, `email`, `nome`, `nomeUtente`, `password`, `ruoli`, `telefono`, `gruppo`) VALUES ('08394170-a1eb-11ef-8666-00505600ed76',NULL,NULL,'info@omegaambiente.it',NULL,'omega','omega24','Esecutore',NULL,'omega'),
('393d1389-a1eb-11ef-8666-00505600ed76',NULL,NULL,'info@piemonte.it',NULL,'piemonte','piemonte24','Ente',NULL,'piemonte'),
('f5b6563b-a1ea-11ef-8666-00505600ed76',NULL,NULL,'claudio.viggiano@fratriaservizi.com',NULL,'ciclat','ciclat24','Admin',NULL,'ciclat'),
('f9efe431-a1ea-11ef-8666-00505600ed76',NULL,NULL,'viggiano.omega@gmail.com',NULL,'mario','mario24','Tecnico',NULL,'ciclat');

INSERT INTO `Adempimento` (`uuid`, `createdAt`, `deletedAt`, `hidden`, `ownerUsername`, `readonly`, `calcolo`, `dataDiRiferimento`, `giorni`, `nome`, `relativoA`, `ricorrenza`, `commessa_uuid`, `ownerGroup`) VALUES ('084a263d-f466-41d2-928e-c0200d924a1f','2025-01-27 19:34:20.649000',NULL,0x00,'ciclat',0x00,0,'protocollo RPF',5,'Caricamento VdC',0,1,NULL,NULL),
('0c16793d-2462-44de-b066-f330d954e58c','2025-01-16 12:58:14.018000',NULL,0x00,'ciclat',0x00,1,'accettazione',7,'Caricamento PDA',1,0,NULL,NULL),
('7f46218f-8021-4541-9c8a-6ca5105d87ce','2025-01-27 22:24:59.815000',NULL,0x00,'ciclat',0x00,0,'protocollo',15,'Caricamento Livelli di Servizio',0,1,NULL,NULL),
('df36af19-bdd9-4b39-afbf-7b1a154c68f6','2025-01-27 19:25:30.091000',NULL,0x00,'ciclat',0x00,0,'protocollo RPF',25,'Caricamento POA',0,1,NULL,NULL);

INSERT INTO `ModelloAvvisoScadenza` (`uuid`, `createdAt`, `deletedAt`, `hidden`, `ownerUsername`, `readonly`, `noteAdUsoInterno`, `oggetto`, `testo`, `azione`, `giorniAnticipo`, `nomeBreve`, `adempimento_uuid`, `ownerGroup`) VALUES ('0d02a899-dc3d-4cee-9989-5b7f0727be09','2025-01-27 22:26:07.211000',NULL,0x00,'ciclat',0x00,'','ATTENZIONE! Scadenza caricamento file \"Livelli di servizio\" per RPF {{codiceRPF}}','Buongiorno,\n\nla presente mail per comunicare che il 15 {{nomeMese}}, ovvero fra {{numeroGiorni}} giorni, scade il termine di caricamento relativo al caricamento del \"Livelli di servizio\" per RPF  {{codiceRPF}}.\n\n\nAccedere al portale di gestione gare per concludere l\'attività: \n<a href=\"https://ciclatfm.it\">CICLAT FM: www.ciclatfm.it</a>.\n\nMessaggio in riferimento a:\n- Commessa: {{nomeCommessa}}\n- Ente: {{nomeEnte}}\n- Lotto: {{codiceLotto}}\n\nBuona giornata',3,3,'caricamento Liv Serv','7f46218f-8021-4541-9c8a-6ca5105d87ce',NULL),
('1c0af2a1-9a09-4ddc-8471-d1bbf759d6d3','2025-01-27 19:32:35.319000',NULL,0x00,'ciclat',0x00,'','ATTENZIONE! Scadenza caricamento POA per edifici RPF {{codiceRPF}}','Buongiorno,\n\nla presente mail per comunicare che il 25 {{nomeMese}} scade il termine di caricamento relativo al caricamento dei POA per edifici del RPF {{codiceRPF}}.\n\n\nAccedere al portale di gestione gare per concludere l\'attività: \n<a href=\"https://ciclatfm.it\">CICLAT FM: www.ciclatfm.it</a>.\n\nMessaggio in riferimento a:\n- Commessa: {{nomeCommessa}}\n- Ente: {{nomeEnte}}\n- Lotto: {{codiceLotto}}\n\nBuona giornata\n',1,3,'caricamento POA','df36af19-bdd9-4b39-afbf-7b1a154c68f6',NULL),
('715c02c9-6ca6-4b69-ac33-f57cc52ffbc4','2025-01-27 19:38:32.264000',NULL,0x00,'ciclat',0x00,'','ATTENZIONE! Scadenza caricamento VdC per RPF {{codiceRPF}}','Buongiorno,\n\nla presente mail per comunicare che il 5 {{nomeMese}}, ovvero fra {{numeroGiorni}} giorni, scade il termine di caricamento relativo al caricamento del file VdC per RPF {{codiceRPF}}.\n\n\nAccedere al portale di gestione gare per concludere l\'attività: \n<a href=\"https://ciclatfm.it\">CICLAT FM: www.ciclatfm.it</a>.\n\nMessaggio in riferimento a:\n- Commessa: {{nomeCommessa}}\n- Ente: {{nomeEnte}}\n- Lotto: {{codiceLotto}}\n\nBuona giornata\n',2,3,'caricamento VdC','084a263d-f466-41d2-928e-c0200d924a1f',NULL),
('8293aceb-8124-46e8-87ef-670aed829035','2025-01-16 15:52:22.029000',NULL,0x00,'ciclat',0x00,'','ATTENZIONE! Scadenza caricamento PDA per RPF {{codiceRPF}}','Buongiorno,\n\nla presente mail per comunicare che tra {{numeroGiorni}} giorni scade il termine di caricamento del PDA relativo a RPF {{codiceRPF}} relativo a {{nomeCommessa}}. Si prega di procedere urgentemente.\n\nAccedere al portale di gestione gare per concludere l\'attività: \n<a href=\"https://ciclatfm.it\">CICLAT FM: www.ciclatfm.it</a>.\n\nMessaggio in riferimento a:\n- Commessa: {{nomeCommessa}}\n- Ente: {{nomeEnte}}\n- Lotto: {{codiceLotto}}\n\nBuona giornata',0,3,'carica pda','0c16793d-2462-44de-b066-f330d954e58c',NULL);
									

INSERT INTO `Commessa` VALUES
('936ce8f8-a96b-4c26-840e-12cc8c93ce8d','2025-02-12 13:43:53.986000',NULL,'\0','ciclat','ciclat','\0','2025-02-12 13:43:53.986000',NULL,'2025-02-12','2028-01-29','','c1sdcs','',0);

INSERT INTO `Ente` VALUES
('ab7cac92-6595-4170-b655-d655eb27689c','2025-02-12 13:44:16.071000',NULL,'\0','ciclat','ciclat','\0','2025-02-12 13:44:16.071000','','Piemonte','');

INSERT INTO `Lotto` VALUES
('3aacda18-b153-4c11-b1f3-86b926fce3ba','2025-02-12 13:44:05.093000',NULL,'\0','ciclat','ciclat','\0','2025-02-12 13:44:05.093000','L01','','','936ce8f8-a96b-4c26-840e-12cc8c93ce8d');

