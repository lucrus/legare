#!/bin/bash
#

echo Scegli su quale server eeguire il deploy
echo
echo 1 - dev.ciclatfm.it
echo
echo 2 - ciclatfm.it
echo

read S

case $S in
  "1" )
    SERVER=dev.ciclatfm.it
    ;;
  "2" )
    SERVER=ciclatfm.it
    ;;
  * )
    echo Uh?
    exit 1
    ;;
esac

MD=`echo -n ${SERVER} | openssl md5 - | cut -d' ' -f2`
CONFIRM=$(( $((0x${MD})) % 999))
CONFIRM=${CONFIRM#-}

echo
echo #################################
echo
echo Conferma la scelta di eseguire il
echo deploy sul server "${SERVER}" 
echo con il codice di conferma $CONFIRM
echo
echo #################################
echo
echo -n "Codice di conferma: "
read C

if [ "$C" != "$CONFIRM" ] ; then
  echo "Operazione annullata, deploy non eseguito"
  exit 1
fi

git log > src/main/resources/assets/gitlog@deploy.txt

rm -rf target
mvn package

scp -P 31222 target/legare.war "${SERVER}":~/legare-$(date +%F).war


