#!/bin/bash
#

mvn clean
mvn package

startTomcat() {

  cd ~/programmi/tomcat
  ./bin/startup.sh
  cd -

}

tomcatPids() {
  ps -u "${CURRUSER}" --format pid,comm,args | grep java | grep tomcat | tr -s ' ' | sed -e 's/^ //' | cut -d' ' -f1
}

stopTomcat() {
  
  cd ~/programmi/tomcat
  ./bin/shutdown.sh
  cd -

  CURRUSER=`id -un`
  LIVINGPIDS=`tomcatPids`
  while [ "$LIVINGPIDS" != "" ] ; do
    kill ${LIVINGPIDS}
    sleep 1
    LIVINGPIDS=`tomcatPids`
  done
}

stopTomcat

rm -rf ~/programmi/tomcat/webapps/legare*
cp -a target/legare.war ~/programmi/tomcat/webapps

if [ "$2" == "0db" ] ; then
  cat src/main/resources/resetdb.sql | mysql
  
  startTomcat

  echo -n "Waiting for the database schema..."
 
  SCHEMAOK=0
  while [ $SCHEMAOK -eq 0 ] ; do
    echo -n .
    sleep 1
    SCHEMAOK=`cat src/main/resources/waitdbschema.sql | mysql 2>/dev/null | grep uuid | wc -l`
  done
  echo
  cat src/main/resources/populatedb.sql | mysql

  if [ "$1" != "start" ]  ; then
    stopTomcat
  fi
elif [ "$1" == "start" ] ; then
  startTomcat
fi


